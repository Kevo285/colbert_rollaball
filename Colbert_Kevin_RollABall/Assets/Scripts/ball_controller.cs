﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ball_controller : MonoBehaviour
{

    public float speed;
    private int count;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        winText.text = "";
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
        setCountText();


    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count +2;
            setCountText();
        }
        else if (other.gameObject.CompareTag("Pick Down"))
        {
            other.gameObject.SetActive(false);
            count = count - 1;
            setCountText();
        }
    }


    void setCountText ()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 14)
        {
            winText.text = "You Win!";
        }
        else if (count == -6)
        {
            winText.text = "Why would you do that?";
        }
    }
}
