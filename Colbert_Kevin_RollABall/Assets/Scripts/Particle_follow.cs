﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle_follow : MonoBehaviour
{


    public GameObject particle;

    private Vector3 offset;



    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - particle.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = particle.transform.position + offset;
    }
}
